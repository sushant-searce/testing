import airflow
from airflow import DAG
from airflow.contrib.operators.dataflow_operator import DataflowTemplateOperator
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.mysql_hook import MySqlHook
from airflow.models import Variable
import pprint
from google.cloud import bigquery
import os
from datetime import timedelta, date
from airflow.operators.dummy_operator import DummyOperator 
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.email_operator import EmailOperator


mysql_source_table_name = "<TABLE_NAME>"

mysql_hook = MySqlHook(mysql_conn_id="airflow_db", schema="composer-1-**-2-airflow-1-**-**-********")
connection = mysql_hook.get_conn()
cursor = connection.cursor()

default_args = {
    'owner': 'airflow',
    # 'start_date': airflow.utils.dates.days_ago(0),
    'start_date': datetime(2021, 2, 7),
    'email_on_retry': False,
    'dataflow_default_options': {
        'project': '< PROJECT_NAME >',
        'region': '< REGION >',
        'zone': '<ZONE>',
        'tempLocation': 'gs://< TEMP_BUCKET >/temp/',
        'network': "prod-********-vpc",
        'subnetwork': "https://www.googleapis.com/compute/v1/projects/< PROJECT_NAME >/regions/< REGION >/subnetworks/< SUBNET >",
        'ipConfiguration': "WORKER_IP_UNSPECIFIED",
        'maxWorkers': 1,
        'numWorkers': 1,
        'machineType': "n1-standard-4",
        "disk_size_gb":200,
        'workerZone': "asia-south1-a"
         }
     }


dag = DAG(dag_id="<DAG_ID>",schedule_interval='30 19 * * *',catchup=False,default_args=default_args)

def start_batch(**kwargs):
    cursor.execute('select * from metadata where source_table_name="{}"'.format(mysql_source_table_name))
    results=cursor.fetchall()

    for row in results:
        source_type=row[0]
        pipeline_type=row[1]
        source_table_name=row[2]
        raw_table_name=row[3]
        merge_table_name=row[4]
        primary_key=row[5]
        partition_col=row[6]
        partition_type=row[7]
        last_merge_ts=row[8]
        startDate = row[9]
        batchDays = row[10]
        sync_lock=row[11]
        last_raw_partition_date=row[12]
        last_dag_start_execution_time=row[13]
        last_dag_end_execution_time=row[14]
        last_dag_successful_execution_time=row[15]
        
    if sync_lock == 0:
        cursor.execute('update metadata set sync_lock=1,last_dag_start_execution_time="{}" where source_table_name="{}"'.format(datetime.now(),mysql_source_table_name)) 
        connection.commit()

        # kwargs['ti'].xcom_push(key='startDate', value=startDate)
        kwargs['ti'].xcom_push(key='batchDays', value=batchDays)
        kwargs['ti'].xcom_push(key='raw_table_name', value=raw_table_name)
        kwargs['ti'].xcom_push(key='merge_table_name', value=merge_table_name)
        kwargs['ti'].xcom_push(key='primary_key', value=primary_key)
        kwargs['ti'].xcom_push(key='partition_col', value=partition_col)
        kwargs['ti'].xcom_push(key='partition_type', value=partition_type)
        kwargs['ti'].xcom_push(key='last_merge_ts', value=last_merge_ts)
        kwargs['ti'].xcom_push(key='last_raw_partition_date', value=last_raw_partition_date)

        endDate  = startDate + timedelta(days=batchDays)
        today = date.today()
        if endDate >= today:
            endDate = today
        kwargs['ti'].xcom_push(key='endDate', value=str(endDate))
        # print("Dates all   " , startDate, "  ", batchDays , "     ",endDate)
    
        mysql_source_db_hook = MySqlHook(mysql_conn_id="Mysql-source-database", schema="<DATABASE_NAME>")
        connection_source_db = mysql_source_db_hook.get_conn()
        cursor_source = connection_source_db.cursor()

        dynamic_query = "select"

        cursor_source.execute('desc {}'.format(mysql_source_table_name))  
        results2=cursor_source.fetchall()
    
        for row in results2:
            if row[1] == "date" or row[1] == "datetime":
                dynamic_query = dynamic_query + " CONVERT("+ row[0] + ",CHAR) as " + row[0] + ","
            elif row[1] == "timestamp":
                dynamic_query = dynamic_query + " UNIX_TIMESTAMP(" + row[0] + ") as " + row[0] + ","
            else:
                dynamic_query = dynamic_query + " " + row[0] + ","
        
        dynamic_query = dynamic_query + "UNIX_TIMESTAMP(updated_at)*1000 as __ts_ms, 'false' as __deleted"

        dynamic_query = dynamic_query + " from " + mysql_source_table_name + " where updated_at >= '" + str(startDate) + "' AND updated_at < '" + str(endDate) +"'"
        cursor_source.execute(dynamic_query)
        countval = cursor_source.rowcount
        print(dynamic_query)
        kwargs['ti'].xcom_push(key='countval', value=countval)
        kwargs['ti'].xcom_push(key='dynamic_query', value=dynamic_query) 
        return 'data_exists'   
    else:
        return 'batch_dag_in_progress'

def checkcountval(**kwargs):
    countval = kwargs['ti'].xcom_pull(key='countval', task_ids='start_batch')
    print(countval)
    if (countval==0):
        return 'no_batch_load'
    else:
        return 'dataflow_batch_load'

data_exists_op = BranchPythonOperator(
    task_id='data_exists',
    provide_context=True,
    python_callable=checkcountval,
    dag=dag
    )

def end_batch(**kwargs):
    endDate = kwargs['ti'].xcom_pull(key='endDate', task_ids='start_batch')
    cursor.execute('update metadata set last_batch_ts="{}" where source_table_name="{}"'.format(endDate,mysql_source_table_name))
    connection.commit()

def start_batch_merge_process(**kwargs):
    raw_table_name = kwargs['ti'].xcom_pull(key='raw_table_name', task_ids='start_batch')
    merge_table_name = kwargs['ti'].xcom_pull(key='merge_table_name', task_ids='start_batch')
    primary_key = kwargs['ti'].xcom_pull(key='primary_key', task_ids='start_batch')
    partition_col = kwargs['ti'].xcom_pull(key='partition_col', task_ids='start_batch')
    partition_type = kwargs['ti'].xcom_pull(key='partition_type', task_ids='start_batch')
    last_merge_ts = kwargs['ti'].xcom_pull(key='last_merge_ts', task_ids='start_batch')
    last_raw_partition_date = kwargs['ti'].xcom_pull(key='last_raw_partition_date', task_ids='start_batch')

    print("Merge process started for raw table-> " + raw_table_name + " and merge table-> " + merge_table_name)

    # Construct a BigQuery client object.
    client = bigquery.Client()
    raw_table = client.get_table(raw_table_name)  # Make an API request.
    main_table = client.get_table(merge_table_name)  # Make an API request.
    main_cols = list(m.name for m in main_table.schema)

    alter_cols = []
    skip_cols = ['__ts_ms','__deleted']
    for r in raw_table.schema:
        if r.name not in skip_cols:
            if r.name not in main_cols:
                if r.field_type == "INTEGER":
                    field_tmp = "INT64"
                    alter_cols.append("ADD COLUMN {} {}".format(r.name,field_tmp))
                else:
                    alter_cols.append("ADD COLUMN {} {}".format(r.name,r.field_type))
    if len(alter_cols) > 0:
        separator = ', '
        temp_str = separator.join(alter_cols)
        print("""ALTER TABLE `{}` {} ;""".format(merge_table_name,temp_str))
        query_job = client.query("""ALTER TABLE `{}` {} ;""".format(merge_table_name,temp_str))
    else:
        print("Columns are same in both raw and main table!")

    mysql_source_db_hook = MySqlHook(mysql_conn_id="Mysql-source-database", schema="<DATABASE_NAME>")
    connection_source_db = mysql_source_db_hook.get_conn()
    cursor_source = connection_source_db.cursor()

    cursor_source.execute('desc {}'.format(mysql_source_table_name))  
    results2=cursor_source.fetchall()
    
    main_table = client.get_table(merge_table_name)  # to get the updated list of columns from main table
    separator = ', '
    insert_cols_list = list(m.name for m in main_table.schema)
    update_cols_list = list(m.name + "=S." + m.name for m in main_table.schema)
    insert_cols =  str(separator.join(insert_cols_list))
    insert_cols_values =  str(separator.join(insert_cols_list))
    update_cols = str(separator.join(update_cols_list))

    for row in results2:
        if row[1] == "timestamp":
            insert_cols_values_str = "TIMESTAMP_ADD("+row[0] + ",interval 330 minute)"
            update_cols_str = "TIMESTAMP_ADD(S." +row[0] + ",interval 330 minute)"

            if row[0] in insert_cols_values:
                insert_cols_values = str.replace(insert_cols_values,row[0],insert_cols_values_str)
                
            if row[0]+'=' in update_cols:
                update_cols = str.replace(update_cols,"S."+row[0],update_cols_str)

    date_format = '"%Y-%m-%d"'

    if(partition_type == 'DATE'):
        query_job = client.query("select max(__ts_ms), FORMAT_DATE({},min({})), FORMAT_DATE({},max({})), FORMAT_TIMESTAMP({},min(_PARTITIONTIME)) from {} where __ts_ms > {} and _PARTITIONTIME >= '{}';".format(date_format, partition_col, date_format, partition_col, date_format, raw_table_name, last_merge_ts, last_raw_partition_date))
        results = query_job.result()
        for row in results:
            max_cdc_ts = row.f0_
            partition_date1=row.f1_
            partition_date2=row.f2_
            PARTITIONTIME_min=row.f3_
            print(max_cdc_ts)
            print(partition_date1)
            print(partition_date2)
            print(PARTITIONTIME_min)
    elif(partition_type == 'DATETIME'):
        query_job = client.query("select max(__ts_ms), FORMAT_DATETIME({},min({})), FORMAT_DATETIME({},DATETIME_ADD(max({}), INTERVAL 30 HOUR)), FORMAT_TIMESTAMP({},min(_PARTITIONTIME)) from {} where __ts_ms > {} and _PARTITIONTIME >= '{}';".format(date_format, partition_col, date_format, partition_col, date_format, raw_table_name, last_merge_ts, last_raw_partition_date))
        results = query_job.result()
        for row in results:
            max_cdc_ts = row.f0_
            partition_date1=row.f1_
            partition_date2=row.f2_
            PARTITIONTIME_min=row.f3_
            print(max_cdc_ts)
            print(partition_date1)
            print(partition_date2)
            print(PARTITIONTIME_min)
    elif(partition_type == 'TIMESTAMP'):
        query_job = client.query("select max(__ts_ms), FORMAT_TIMESTAMP({},min({})), FORMAT_TIMESTAMP({},TIMESTAMP_ADD(max({}), INTERVAL 30 HOUR)), FORMAT_TIMESTAMP({},min(_PARTITIONTIME)) from {} where __ts_ms > {} and _PARTITIONTIME >= '{}';".format(date_format, partition_col, date_format, partition_col, date_format, raw_table_name, last_merge_ts, last_raw_partition_date))
        results = query_job.result()
        for row in results:
            print(row)
            max_cdc_ts = row.f0_
            partition_date1=row.f1_
            partition_date2=row.f2_
            PARTITIONTIME_min=row.f3_
            print(max_cdc_ts)
            print(partition_date1)
            print(partition_date2)
            print(PARTITIONTIME_min)
    else:
        query_job = client.query("select max(__ts_ms), FORMAT_TIMESTAMP({},min(_PARTITIONTIME)) from {} where __ts_ms > {} and _PARTITIONTIME >= '{}';".format(date_format, raw_table_name, last_merge_ts, last_raw_partition_date))
        results = query_job.result()
        for row in results:
            max_cdc_ts = row.f0_
            PARTITIONTIME_min=row.f1_
            print(max_cdc_ts)
            print(PARTITIONTIME_min)

    max_cdc_ts = max_cdc_ts - 60000
    
    print("Merge query execution in progress.....")

    if(partition_col != ''):
        merge_query = client.query("""MERGE `{}` T
                USING (SELECT *
                    FROM (SELECT *, ROW_NUMBER() over(PARTITION BY {} order by __ts_ms desc) AS row_num
                        FROM `{}` WHERE __ts_ms >{} AND __ts_ms <= {} and _PARTITIONTIME >= '{}') dedup where row_num = 1) S
                ON T.{} = S.{} AND T.{} BETWEEN '{}' AND '{}'
                WHEN MATCHED AND S.__deleted='true' THEN
                DELETE
                WHEN MATCHED THEN
                UPDATE SET {}
                WHEN NOT MATCHED THEN
                INSERT ({}) VALUES({});""".format(
                merge_table_name,primary_key,raw_table_name,last_merge_ts,max_cdc_ts,PARTITIONTIME_min,primary_key,primary_key, partition_col, partition_date1, partition_date2, update_cols,insert_cols,insert_cols_values))
                    
        merge_query.result() 

    else:
        merge_query = client.query("""MERGE `{}` T
                USING (SELECT *
                    FROM (SELECT *, ROW_NUMBER() over(PARTITION BY {} order by __ts_ms desc) AS row_num
                        FROM `{}` WHERE __ts_ms >{}  AND __ts_ms <= {} AND _PARTITIONTIME >= '{}') dedup where row_num = 1) S
                ON T.{} = S.{}
                WHEN MATCHED AND S.__deleted='true' THEN
                DELETE
                WHEN MATCHED THEN
                UPDATE SET {}
                WHEN NOT MATCHED THEN
                INSERT ({}) VALUES({});""".format(
                merge_table_name,primary_key,raw_table_name,last_merge_ts,max_cdc_ts,PARTITIONTIME_min,primary_key,primary_key,update_cols,insert_cols,insert_cols_values))

        merge_query.result()

    kwargs['ti'].xcom_push(key='max_cdc_ts', value=max_cdc_ts)
    kwargs['ti'].xcom_push(key='PARTITIONTIME_min', value=PARTITIONTIME_min)
    print("Merge process completed for raw table-> " + raw_table_name + " and merge table-> " + merge_table_name)

def update_sync_lock(context):
    update_sync_lock = cursor.execute("UPDATE metadata SET sync_lock=0 where source_table_name='{}';".format(mysql_source_table_name))
    connection.commit()  

def end_batch_merge_process(**kwargs):
    max_cdc_ts = kwargs['ti'].xcom_pull(key='max_cdc_ts', task_ids='start_batch_merge_process')
    PARTITIONTIME_min = kwargs['ti'].xcom_pull(key='PARTITIONTIME_min', task_ids='start_batch_merge_process')

    if(max_cdc_ts is not None and PARTITIONTIME_min is not None):
        update_metadata_query = cursor.execute("UPDATE metadata SET last_merge_ts={}, last_raw_partition_date='{}' where source_table_name='{}';".format(max_cdc_ts, PARTITIONTIME_min,mysql_source_table_name))
        connection.commit()
        print("Updated the metadata table!")
    else:
        print("Nothing to update in the metadata table!")

def success(**kwargs):
    update_metadata_query = cursor.execute("UPDATE metadata SET last_dag_successful_execution_time='{}', last_dag_end_execution_time='{}',sync_lock=0 where source_table_name='{}';".format(datetime.now(), datetime.now(),mysql_source_table_name))
    connection.commit()
    print("All tasks are succeeded!")

def no_batch_data(**kwargs):
    endDate = kwargs['ti'].xcom_pull(key='endDate', task_ids='start_batch')
    update_metadata_query = cursor.execute("UPDATE metadata SET last_batch_ts='{}',last_dag_successful_execution_time='{}', last_dag_end_execution_time='{}',sync_lock=0 where source_table_name='{}';".format(endDate,datetime.now(), datetime.now(),mysql_source_table_name))
    connection.commit()
    print("There is no data to load in Bigquery!")

def failed(**kwargs):
    update_metadata_query = cursor.execute("UPDATE metadata SET last_dag_failed_execution_time='{}', last_dag_end_execution_time='{}',sync_lock=0 where source_table_name='{}';".format(datetime.now(), datetime.now(),mysql_source_table_name))
    connection.commit()
    print("One or more tasks failed in the DAG..please check!")

start_batch_op = BranchPythonOperator(
    task_id='start_batch',
    python_callable=start_batch,
    provide_context=True,
    dag=dag)

end_batch_op = PythonOperator(
    task_id='end_batch',
    provide_context=True,
    python_callable=end_batch,
    dag=dag
    )

DataflowOperator = DataflowTemplateOperator(
    task_id='dataflow_batch_load',
    job_name='airflow',
    template='gs://dataflow-templates-us-central1/latest/Jdbc_to_BigQuery',
    parameters={
        'connectionURL': "jdbc:mysql://< MYSQL_HOST >:3306/<DATABASE_NAME>?user=cdcuser&password='{}'".format(Variable.get("password")),
        'driverClassName': "com.mysql.cj.jdbc.Driver",
        'query': "{{ti.xcom_pull(key='dynamic_query', task_ids='start_batch')}}",
        'outputTable': "********-analytics-prod:{}".format("{{ti.xcom_pull(key='raw_table_name', task_ids='start_batch')}}"),
        'driverJars': "gs://********-dataflow-test-bucket/mysql-connector-java-8.0.16.jar",
        'bigQueryLoadingTemporaryDirectory': "gs://********-dataflow-test-bucket/temp2/",
        'username': "cdcuser",
        'password': "{}".format(Variable.get("password"))
    },
    gcp_conn_id='dataflow_permissions',
    dag=dag
    )

stop_op = DummyOperator(task_id='batch_dag_in_progress', dag=dag)

merge_process_op = PythonOperator(
    task_id='start_batch_merge_process',
    provide_context=True,
    python_callable=start_batch_merge_process,
    on_failure_callback=update_sync_lock,
    dag=dag
    )

end_merge_op = PythonOperator(
    task_id='end_batch_merge_process',
    provide_context=True,
    python_callable=end_batch_merge_process,
    dag=dag
    )

check_task_success_op = PythonOperator(
    task_id='check_task_success',
    provide_context=True,
    python_callable=success,
    trigger_rule='all_success',
    on_failure_callback=update_sync_lock,
    dag=dag
    )

no_batch_load_op = PythonOperator(
    task_id='no_batch_load',
    provide_context=True,
    python_callable=no_batch_data,
    on_failure_callback=update_sync_lock,
    dag=dag
    )


check_task_failure_op = PythonOperator(
    task_id='check_task_failure',
    provide_context=True,
    python_callable=failed,
    trigger_rule='one_failed',
    on_failure_callback=update_sync_lock,
    dag=dag
    )

start_batch_op >> [data_exists_op,stop_op]
data_exists_op >> [DataflowOperator,no_batch_load_op] 
DataflowOperator >> end_batch_op >> merge_process_op >> end_merge_op >> [check_task_success_op,check_task_failure_op]
