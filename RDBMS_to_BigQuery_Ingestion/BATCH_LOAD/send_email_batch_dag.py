from datetime import datetime, timedelta
from airflow.models import DAG
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
import airflow
from datetime import datetime, timedelta
from airflow.utils.email import send_email
from airflow.operators.email_operator import EmailOperator

dag_interval_in_hrs = 24

default_args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(0),
    'email_on_retry': False
}

dag = DAG(dag_id='send_email_batch_dag',schedule_interval='0 2 * * *',catchup=False,default_args=default_args)

def failed_tasks_email(**context):
    failed_dags_list=[]
    no_dags_executed_list = []
    mysql_hook = MySqlHook(mysql_conn_id="airflow_db", schema="composer-1-**-2-airflow-1-10-**-*******")
    request = "select * from metadata where pipeline_type='BATCH'"
    connection = mysql_hook.get_conn()
    cursor = connection.cursor()
    cursor.execute(request)
    sources = cursor.fetchall()
    
    for source in sources:
        last_dag_successful_execution_time = source[15]
        last_dag_failed_execution_time = source[17]

        
        if (datetime.now()-timedelta(hours=dag_interval_in_hrs)) > max(last_dag_successful_execution_time, last_dag_failed_execution_time):  ## If no dag is executed in last interval, send mail
            no_dags_executed_list.append(source[2])
        
        elif last_dag_failed_execution_time > last_dag_successful_execution_time:                                                             ## If dag was failed in last interval, send mail
              failed_dags_list.append(source[2])
    
    if no_dags_executed_list:

         title = "Airflow alert"
         body = """
            Hi Everyone, <br>
            <br>
            There's been an error in DAGs.<br>
            No Dag is executed in last {} hours for below tables:<br>
            {}<br>
            <br>
            Forever yours,<br>
            Airflow bot <br>
            """.format(dag_interval_in_hrs,"\n".join(no_dags_executed_list))

         send_email_op = EmailOperator (
                                        dag=dag,
                                        task_id="send_email_dags_not_executed",
                                        to=["*********@email.com","*********@email.com","*********@email.com"],
                                        subject=title,
                                        html_content=body
                                     )

         send_email_op.execute(context)

    if failed_dags_list:
    
            title_v1 = "Airflow alert"
            body_v1 = """
                   Hi Everyone, <br>
                   <br>
                   There's been an error in DAGs for below tables:<br>
                   {}<br>
                   <br>
                   Forever yours,<br>
                   Airflow bot <br>
                   """.format("\n".join(failed_dags_list))

            send_email_op_v1 = EmailOperator (
                                       dag=dag,
                                       task_id="send_email_failed_tasks",
                                       to=["*********@email.com","*********@email.com","*********@email.com"],
                                       subject=title_v1,
                                       html_content=body_v1
                                     )

            send_email_op_v1.execute(context)
    

failed_tasks_email_op = PythonOperator(
    task_id='failed_tasks_mail',
    provide_context=True,
    python_callable=failed_tasks_email,
    dag=dag
    )

failed_tasks_email_op
