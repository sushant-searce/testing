from google.cloud import bigquery
from google.cloud import storage
import airflow
from airflow.hooks.mysql_hook import MySqlHook
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
import pprint
import os
from datetime import timedelta, date
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.email_operator import EmailOperator

mysql_hook = MySqlHook(mysql_conn_id="airflow_db", schema="composer-*-**-*-airflow-*-**-**-*******")
connection = mysql_hook.get_conn()
cursor = connection.cursor()

default_args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(0),
    'email_on_retry': False
}

dag = DAG(dag_id='batch_master_dag',schedule_interval='0 */1 * * *',catchup=False,default_args=default_args)

def create_dag_files(**kwargs):
    cursor.execute('select * from metadata where create_dag=0 and pipeline_type="BATCH"')
    results=cursor.fetchall()

    for row in results:
        source_table_name=row[2]
        source_database=row[18]
        print("Creating DAG file for table : " +source_table_name)
        storage_client = storage.Client()
        bucket = storage_client.get_bucket('< BUCKET NAME >')
        blob = bucket.blob('template_dag/batch_dag_template.py')
        downloaded_blob = blob.download_as_string()
        downloaded_blob = downloaded_blob.decode("utf-8") 
        dag_id = "batch_"+source_table_name+"_dag"
        tmp_string = downloaded_blob.replace("<DAG_ID>",dag_id)
        updated_string = tmp_string.replace("<TABLE_NAME>",source_table_name)
        updated_final_string = updated_string.replace("<DATABASE_NAME>",source_database)
        blob2 = bucket.blob('dags/'+dag_id+'.py')
        upload_file = blob2.upload_from_string(updated_final_string,content_type='text/x-python')

        # Update the create_flag to 1
        cursor.execute('update metadata set create_dag=1 where source_table_name="{}"'.format(source_table_name))
        connection.commit()

def send_email(context):
    send_email_task_op = EmailOperator (
                               dag=dag,
                               task_id="send_email_task",
                               to=["< ***********@email.com > ","< ***********@email.com >"],
                               subject="BATCH Master Dag Failed",
                               html_content='<h3>Please check the BATCH Master DAG</h3>')

    send_email_task_op.execute(context)

create_dag_files_op = PythonOperator(
    task_id='create_dag_files',
    provide_context=True,
    python_callable=create_dag_files,
    on_failure_callback=send_email,
    dag=dag
    )

create_dag_files_op
