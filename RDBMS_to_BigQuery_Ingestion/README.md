*****Composer and DAG setup*****

Create a composer with required configuration and whitelist your IP in the web server configuration to see the web interface.
	
**For increasing the node count of the composer :**
  	
	First in GKE console, in the default node pool, enable auto scaling and give the minimum and maximum number of nodes.
	Then in the composer UI, Increase the node count of the composer to the required count.


**For accessing the metadata database of airflow on composer :**

    Connect to the GKE cluster from the terminal
    Whitelist your IP in GKE master authorized network and save the changes.
    
    From Terminal SSH into the GKE cluster and enter below command:

      gcloud container clusters get-credentials < GKE_CLUSTER_NAME > --zone < ZONE > --project < PROJECT_NAME >



Login into GKE cluster and check all the running pods :

kubectl get po --all-namespaces


**Access MySQL to craete Metadata Table For MySQL -**

    SSH to one of the node
    Run the MySQL CLI
    Verify if you access to the database


    Replace the values in the below command :

    kubectl exec \
        -it airflow-worker-***** \
        --namespace=composer-***** -- \
          mysql \
            -u root \
            -p \
            -h airflow-sqlproxy-*****.*****


    Get the mysql password here 

        Access Airflow web UI -> http://< Public ip of vm instance >:8080/admin
        Go to Admin -> Configuration -> sql_alchemy_conn -> < you get the password >:


MySQL JDBC URL ->

    mysql+mysqldb://xxxxx:xxxxxxxxxxxxxxxxxxxxxxxxxxx@127.0.0.1/composer-*-**-*-airflow-*-**-**-******?charset=utf8


**Create Metadata Table In Airflow Composer to Refer Scheduling - **

    craete table metadata with provided Schema 

    CREATE TABLE metadata (
        source_type varchar(20),
        pipeline_type varchar(50),
        source_table_name varchar(100),
        raw_table_name varchar(100),
        merge_table_name varchar(100),
        primary_key varchar(50),
        partition_col varchar(50),
        partition_type varchar(50),
        last_merge_ts bigint,
        last_batch_ts date,
        batch_in_days int,
        sync_lock int,
        last_raw_partition_date varchar(50),
        last_dag_start_execution_time datetime,
        last_dag_end_execution_time datetime,
        last_dag_successful_execution_time datetime,
        create_dag int,
        last_dag_failed_execution_time datetime,
        source_database varchar(50)
        );



    INSERT INTO metadata values(
        'MySQL','BATCH','agent','********_raw_data.mysql_raw_********new_agent','********_analytics_data.agent','id','created_at','TIMESTAMP',0,'2020-01-01',1,0,'2020-01-01','2020-01-01','2020-01-01','2020-01-01',0,'2020-01-01','********new'
        );



**MASTER DAG**


- File names -> batch_master_dag.py

- Master DAG is created for both BATCH tables

- DAG files need to be uploaded in the dags folder of the composer bucket

- Master DAG will create DAG files for tables in the dags folder automatically.

- The Master DAG runs for every one hour and will check for table names in the metadata table whose **create_dag** is 0.


- If there is any table whose create_dag is 0, it will create a DAG file for that table in the dags folder using the template file uploaded in the    bucket ( < composer_bucket >/template_dag )
	  
    The template dag file batch_dag_template.py is already uploaded in the bucket.

- For instance if for the agent table the create_dag is 0, it will create a file with the name batch_agent_dag.py using batch_dag_template.py and place it in the dags folder.)

- Now the batch_agent_dag.py will start to run on a schedule of every 5 mins.
  Hence if you have to start DAGs for multiple tables, make an entry in the metadata table with the create_dag=0 and other values required.

- Once the DAG is created for that table, the value of create_dag is updated to 1 so that for the next run of the master DAG, these tables are not taken into consideration.



**CONNECTION IN AIRFLOW UI**

For connecting to the MySQL source database and metadata table of the composer, the connections are given in the airflow  UI and used in the code.

    Go to the airflow webserver UI -> Admin -> Connections -> 
    airflow_db -> this connection is created for accessing the metadata table created in the airflow metadata db of the composer.
    Mysql-source-database -> this connection is used to connect to the MySQL source database.
    dataflow_permissions -> this connection is used to give GCP credentials required for the dataflow job.(service_account_key.json is used)

Created a variable named password in Variables through airflow UI for storing the MySQL source database password.

**SENDING EMAIL FOR DAG FAILURE.**

- DAG FOR SENDING EMAILS -  send_email_batch_dag.py

- To send notifications if the DAG is failed or if the DAG has not been run for a long time, the email DAG will send you an email alert.


- For this set the below environment variables for sendgrid in the composer.
    SENDGRID_MAIL_FROM
    SENDGRID_API_KEY

- Add the email address in send_email_dag.py and also batch_master_dag.py along with batch_master_dag.py
